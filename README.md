# SODA
Magouilleuse des Sections Sportives de l'Ecole Polytechnique.
Auteurs: Paul Bonduelle (Web), Guillaume Couairon (Algorithme), Cyprien Nielly (Relations), Pierre-Jean Grenier (Réseau).

VERSION 1.0 (2017)

## Introduction

Afin d'être affectés dans les différentes sections sportives, les X sont évalués dès leur arrivée sur le plateau et formulent leurs voeux d'affectations. Jusqu'à la promotion 2016, cette affectation était réalisée à la main en prenant en compte les notes et demandes de tout le monde, processus très complexe qui engendrait beaucoup de mécontents. L'introduction d'un processus d'affectation automatisé (SODA) permet de faciliter la tâche du Bureau de la Formation Sportive tout en augmentant la satisfaction des élèves et des coachs.

Les résultats de satisfaction des élèves pour la promotion 2017 ont été les suivants (affectation initiale)

| CHOIX  |	1     |	   2|	    3|	4  |
|:------:|:------:|:---:|:----:|:---:|
|Effectif|	456|	76|	11|	2|
%|	83.67%|	13.94%|	2.02%|	0.37% |

le système SODA s'appuie sur une interface web à laquelle les élèves peuvent formuler leurs voeux et les chefs de section sportives rentrer leurs notes, ainsi qu'un backend en python pour calculer l'affectation optimale.

## Fonctionnement de l’interface utilisateur (Frontend)

Le site (non hébergé ici) est très simple d’utilisation:
La page d’accueil du site est un descriptif de chaque section (cliquer sur une photo pour avoir les détails sur la section). Depuis la page d’accueil, un élève peut cliquer sur “choix sport” pour accéder à la page où il rentrera ses choix après avoir rentré login et mot de passe. Un CDS peut cliquer sur “accès CDS” pour accéder à un tableau récapitulant tous les élèves de la promotion et leur attribuer une note.
Dans la page “choix sport”, l’élève rentre ses 5 choix par ordre de préférence. Seuls les choix possibles pour cet élève lui sont proposé (pas de rugby pour les filles par exemple). Il peut également répondre à un questionnaire lorsqu’il s’inscrit à un sport. On demande à l’élève de mettre 3 choix individuels et 2 collectifs (mais si l’élève ne respecte pas cette règle, tout fonctionne quand même normalement).
Dans la page “accès CDS”, un CDS peut voir le tableau avec tous les élèves de la promotion. En cliquant sur “+” à coté d’un nom, il peut visualiser les réponses au questionnaire si l’élève a répondu.
Toutes les modifications (chiox des sports par les élèves / notes par les CDS) sont enregistrés dans la BDD.



## Fonctionnement de l’algorithme (Backend)

Le problème d’attribution des sections sportives aux élèves est un problème d’optimisation, sous certaines contraintes (nombre minimal de PAX par section, nombre minimal de filles pour former une équipe féminine). On souhaite dans la mesure du possible satisfaire tout le monde, c’est à dire maximiser les choix 1 et 2 mais en se donnant la possibilité d’échanger deux personnes si jamais l’échange est globalement favorable (par exemple, deux choix 2 valent mieux qu’un choix 1 et un choix 5). Chaque élève se voit donc attribuer un score de satisfaction en fonction de la position de sa section dans son classement, et de la note qu’il a obtenue à l’épreuve sportive. On cherche alors à maximiser la somme des satisfactions des élèves, sous les contraintes d’effectifs des sections.

La fonction que l’on cherche à minimiser est $`f(A) = \sum_i^n S(i, A(i))`$ où $`A(i)`$ est la section attribuée à l’élève $`i`$ et $`S(i, a)`$ est le score d’attribution de l’élève $`i`$ en section $`a`$ (plus ce score est haut, moins l’élève $`i`$ est satisfait d’être pris en section $`a`$).
Nous avons choisi de prendre $`S(i, a) = (c_i[a]-p*n_i[a])^2`$ où $`c_i[a]`$ est la place de la section $`a`$ dans le classement de l’élève $`i`$, $`n_i[a]`$ est la note de l’élève $`i`$ aux tests sportifs de la section $`a`$, et $`p`$ est le paramètre d’influence des résultats sportifs dans l’attribution des sections.
Plus la note de l’élève $`i`$ dans son choix 1 est bonne, plus l’attribution est favorable à accorder ce choix à l’élève $`i`$.
La minimisation de cette fonction est réalisée avec une méthode du package scipy, librairie scientifique en python : il s’agit de la méthode linear_sum_assignement, qui résout le problème d’affectation optimale avec l’algorithme hongrois.
https://fr.wikipedia.org/wiki/Probl%C3%A8me_d%27affectation
https://fr.wikipedia.org/wiki/Algorithme_hongrois



La sécurité de SODA a été éprouvée par le BR et la DSI de l’École polytechnique, afin de s’assurer de l’absence de faille connue.
