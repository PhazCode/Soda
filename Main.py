from Promotion import Promotion
#from preprocess import DB_to_dataframe
from scipy.optimize import linear_sum_assignment

#DB_to_dataframe('database.csv')

P = Promotion(df_name='database.csv')

p = 1# influence_cds
S = score_function = lambda placement, notes: (placement + p*(1-notes))**2 # SCORE FUNCTION
# placement[i] = place à laquelle l'élève a mis la section i

P.init_scores_eleves(score_function=S)

M = P.cost_matrix()
print('Start alg')
row_ind, col_ind = linear_sum_assignment(M)

P.repartir_eleves(col_ind)

P.save_attribution()
P.compute_stats()
