import pandas as pd


class Ecole:
    # regroupe tout ce qui concerne quotas, noms...
    sections = pd.DataFrame([
        dict(nom='Aviron', id=0, effectif_max=36, effectif_min=10, effectif_min_F=0),
        dict(nom='Badminton', id=1, effectif_max=32, effectif_min=0, effectif_min_F=0),
        dict(nom='Basket', id=2, effectif_max=44, effectif_min=9, effectif_min_F=9),
        dict(nom='Boxe', id=3, effectif_max=36, effectif_min=0, effectif_min_F=0),
        dict(nom='Equitation', id=4, effectif_max=44, effectif_min=20, effectif_min_F=0),
        dict(nom='Escalade', id=5, effectif_max=36, effectif_min=0, effectif_min_F=0),
        dict(nom='Escrime', id=6, effectif_max=44, effectif_min=0, effectif_min_F=0),
        dict(nom='Foot', id=7, effectif_max=44, effectif_min=16, effectif_min_F=0),
        dict(nom='Hand', id=8, effectif_max=44, effectif_min=10, effectif_min_F=10),
        dict(nom='Judo', id=9, effectif_max=36, effectif_min=0, effectif_min_F=0),
        dict(nom='Natation', id=10, effectif_max=44, effectif_min=0, effectif_min_F=0),
        dict(nom='Raid', id=11, effectif_max=36, effectif_min=0, effectif_min_F=0),
        dict(nom='Rugby', id=12, effectif_max=44, effectif_min=20, effectif_min_F=0),
        dict(nom='Tennis', id=13, effectif_max=28, effectif_min=0, effectif_min_F=0),
        dict(nom='Ultimate', id=14, effectif_max=44, effectif_min=18, effectif_min_F=0),
        dict(nom='Volley', id=15, effectif_max=44, effectif_min=8, effectif_min_F=8)
    ])
    nb_sections = len(sections)
