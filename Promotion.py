import pandas as pd
import numpy as np
from Ecole import Ecole

def pc(x):
    return str(100 * x)[:4] + '%'

class Promotion:
    def __init__(self, df_name='database.csv'):
        self.df = pd.read_csv(df_name)
        self.size = len(self.df)

    def iter(self):
        return self.df.iterrows()

    def init_scores_eleves(self, score_function):
        self.scores = np.zeros((self.size, Ecole.nb_sections))
        for pid, asp in self.iter():
            ordre_preference = [asp['choix ' + str(i)] for i in range(5)]
            notes = [asp['note ' + str(i)] for i in range(5)]
            for j in range(16):
                if not (j in ordre_preference):
                    ordre_preference.append(j)
            ordre_preference = np.array(ordre_preference)
            notes.extend(np.zeros(11))
            placement_section = np.argsort(ordre_preference)
            evaluation_by_section_id = np.zeros(Ecole.nb_sections)
            for i in range(16):
                evaluation_by_section_id[i] = notes[placement_section[i]]
            asp_score = score_function(placement_section, evaluation_by_section_id)
            self.scores[pid] = asp_score

    def cost_matrix(self):
        nb_eleves = len(self.df)
        # correspondance id_matrice to section id
        effectifs_max = [section.effectif_max for index, section in Ecole.sections.iterrows()]
        effectifs_min = [section.effectif_min for index, section in Ecole.sections.iterrows()]
        start_ids = [sum(effectifs_max[:i]) for i in range(Ecole.nb_sections)]

        self.mat_to_section_id = np.repeat(range(Ecole.nb_sections), effectifs_max)
        nb_places = np.sum(effectifs_max)
        delta = nb_places - self.size
        C = np.zeros((nb_places, nb_places))
        for i in range(Ecole.nb_sections):
            column_scores = self.scores[:, i]
            idx, M, m = start_ids[i], effectifs_max[i], effectifs_min[i]
            C[:, idx:idx + m] = np.array(m * [np.concatenate((column_scores, delta * [1e6]))]).transpose()
            C[:, idx + m:idx + M] = np.array((M - m) * [np.concatenate((column_scores, delta * [0]))]).transpose()
        return C

    def repartir_eleves(self, col_ind):
        self.df.loc[:, 'section'] = self.mat_to_section_id[col_ind][:self.size]
        for index, asp in self.iter():
            for i in range(5):
                if asp['choix ' + str(i)] == asp.section:
                    self.df.loc[index, 'choix_attribue'] = i

    def save_attribution(self):
        attribution = [{'matricule': asp.matricule,
                        'nom': asp.nom,
                        'prenom': asp.prenom,
                        'section': Ecole.sections.nom[asp.section],
                        '#choix': asp.choix_attribue + 1} for index, asp in self.iter()]
        pd.DataFrame(attribution).to_csv('attribution.csv')

    def compute_stats(self):
        self.stats = {}
        self.stats['choix_attribue'] = [len([asp for index, asp in self.iter()
                                             if asp.choix_attribue == i]) for i in range(5)]
        self.stats['sections'] = []
        for index, section in Ecole.sections.iterrows():
            # recupere le personnel de la section
            personnel = self.df[self.df.section == index]
            size = len(personnel)
            if size != 0:
                self.stats['sections'].append({'nom': section.nom,
                                               'effectif': size,
                                               'effectif F': len(
                                                   [asp for index, asp in personnel.iterrows() if asp.sexe == 'F']),
                                               '%choix 1': pc(len([asp for index, asp in personnel.iterrows() if
                                                                   asp.choix_attribue == 0]) / size),
                                               '%choix 2': pc(len([asp for index, asp in personnel.iterrows() if
                                                                   asp.choix_attribue == 1]) / size),
                                               })
        for d in self.stats['sections']:
            for key, value in d.items():
                print(key, ' : ', value)
        print('choix attribués :', self.stats['choix_attribue'])
        print('Mecontents : ')
        for index, asp in self.df[self.df.choix_attribue > 1].iterrows():
            print('Asp ', asp.nom, ' en section ', Ecole.sections.loc[asp.section, 'nom'],
                  ' choix {0}'.format(asp.choix_attribue + 1))

        pd.DataFrame(self.stats['sections']).to_csv('sections_stats.csv')
