import _mysql
import pandas as pd
import numpy as np

def DB_to_dataframe(df_name = 'database.csv'):
    db = _mysql.connect(host="localhost", user="root", passwd="password", db="choix_sports")

    db.query("""SELECT id_eleve, nom, prenom, statut, sexe, choix1, choix2, choix3, choix4, choix5 FROM eleves""")
    r = db.store_result()
    eleves = r.fetch_row(maxrows=0)
    # a est un tuple python de dimension 2
    # a[0] contient un tuple qui représente un éleve dont le nom est a[0][1]
    eleves_list = []

    for e in eleves:
        matricule = e[0]
        nom, prenom, statut, sexe = [x.decode('utf-8') for x in e[1:5]]
        choix = np.array([int(e[j]) for j in range(5, 10)])
        notes = []
        # attention, on applique la fonction x -> 9-x aux notes car dans la BDD 10 est la meilleure note (c'est l'inverse dans le code)
        for c in choix:
            db.query("""SELECT note_obtenue FROM note WHERE id_eleve=""" + str(matricule) + """ AND id_sport=""" + str(
                c) + ";")
            r2 = db.store_result()
            a2 = r2.fetch_row()
            try:
                note = int(a2[0][0])/10
            except:
                note = 0  # LOL le nulos
            notes.append(note)
        eleves_list.append({'nom':nom,
                            'prenom': prenom,
                            'matricule': matricule,
                            'sexe': sexe,
                            **{'choix '+str(i): c - 1 for i, c in enumerate(choix)},
                            **{'note '+str(i): c for i, c in enumerate(notes)},
                            'section':-1,
                            'choix_attribue':-1
        })
    pd.DataFrame(eleves_list).to_csv(df_name)


def harmonize_notes(df_name='database.csv'):
    df = pd.read_csv(df_name)
    nb_sections = 16
    notes_by_section = [[] for i in range(16)]
    means, vars = np.zeros(16), np.zeros(16)
    for index, asp in df.iterrows():
        for i in range(5):
            choice_key, note_key = 'choix {0}'.format(i), 'note {0}'.format(i)
            note = df.loc[index, note_key]
            if note != 0:
                notes_by_section[df.loc[index, choice_key]].append(note)
    means = [np.mean(notes) for notes in notes_by_section]
    vars = [np.var(notes) for notes in notes_by_section]
    vars = [x if x > 0 else 1 for x in vars] # Pour les chefs de section qui font de la merde
    print(means)
    print(vars)
    def f(note, sport_id):
        return (note-means[sport_id])
    for index, asp in df.iterrows():
        for j in range(5):
            choice_key, note_key = 'choix {0}'.format(j), 'note {0}'.format(j)
            sport_id, note = df.loc[index, choice_key], df.loc[index, note_key]
            df.loc[index, note_key] = f(note, sport_id)
    df.to_csv(df_name)